package com.elrol.survival_group.commands;

import java.io.IOException;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.elrol.survival_group.core.Main;

public class SurvivalGroup implements CommandExecutor {

	public Main plugin;	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		Player p = (Player)sender;
		UUID pid = p.getUniqueId();
		
		
		if(plugin.pdata.getString(pid.toString()) == null){
			plugin.pdata.set(pid.toString() + ".name", p.getDisplayName());
			plugin.pdata.set(pid.toString() + ".group", "");
			try{
				plugin.pdata.save(plugin.playerData);
			} catch(IOException e){
				e.printStackTrace();
			}
		}
		
		
		if(cmd.getName().equalsIgnoreCase("survivalgroup") && args[0] == null){
			if(!(p instanceof Player)){
				plugin.logger.info("[Info] Only players can use this command");	
			}else{
				p.sendMessage(ChatColor.GOLD + "A list of commands for SurvivalGroup");
				p.sendMessage(ChatColor.YELLOW + "/SurvivalGroup" + ChatColor.WHITE + " : The SurvivalGroup Command List");
				p.sendMessage(ChatColor.YELLOW + "/SurvivalGroup create [Name] <Desc>" + ChatColor.WHITE + " : Create a new Group");
			}	
		}else if(cmd.getName().equalsIgnoreCase("survivalgroup") && args[0].equalsIgnoreCase("create")){
			if(plugin.pdata.getString(pid.toString() + ".group") == null){
				if(args.length == 2){
					plugin.pdata.set(pid.toString() + ".group", args[1]);
					plugin.pdata.set(pid.toString() + ".is_leader", true);
				}else{
					p.sendMessage(ChatColor.GOLD + "Your new group must have a name");
				}
			}else{
				p.sendMessage(ChatColor.GOLD + "You must leave your current group to create a new group");
			}
		}
		return false;
	}

}

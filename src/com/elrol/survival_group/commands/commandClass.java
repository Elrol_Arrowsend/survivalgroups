package com.elrol.survival_group.commands;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.elrol.survival_group.core.Main;


public class commandClass implements CommandExecutor {

	public Main plugin;
	private ArrayList<UUID> coolDown = new ArrayList<UUID>();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		Player p = (Player)sender;
		final UUID pid = p.getUniqueId();
		
		if(args.length != 1){
			p.sendMessage("Invalid Arguments");
			return true;
		}else{
			if(args[0].equalsIgnoreCase("op")){
				if(!coolDown.contains(pid)){
					coolDown.add(pid);
					p.getInventory().addItem(new ItemStack(Material.BAKED_POTATO, 50));
					
					Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable()
					{
						@Override
						public void run()
						{
							coolDown.remove(pid);
						}
					}, 100L);
				}else{
					p.sendMessage("wait till you are cooled down");
				}
			}else{
				p.sendMessage("there is no such kit called" + args);
			}
			
		}
		return false;
	}
	
	

}

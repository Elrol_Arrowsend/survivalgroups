package com.elrol.survival_group.commands;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.elrol.survival_group.core.Main;

public class Commands implements CommandExecutor {

	private Main plugin;
	private String desc = "default description";
	public Commands(Main plugin){
		this.plugin = plugin;
	}
	public static UUID pid;
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
	
		Player p = (Player)sender;
		pid = p.getUniqueId();
		
		if(args.length == 0){
			help(p);
		}
		
		if(args.length == 1){
			if(args[0].equalsIgnoreCase("leave")){leave(p, cmd, s, args);} 
			if(args[0].equalsIgnoreCase("group")){group(p);}
			if(args[0].equalsIgnoreCase("base")){base(p, cmd, s, args);}
			if(args[0].equalsIgnoreCase("setbase")){base(p, cmd, s, args);}
			else if(!args[0].equalsIgnoreCase("leave") && !args[0].equalsIgnoreCase("group") && !args[0].equalsIgnoreCase("base") && !args[0].equalsIgnoreCase("setbase")){p.sendMessage(ChatColor.RED + "Invalid Command");}
		}
		
		if(args.length >= 1){
			if(args[0].equalsIgnoreCase("create")){create(p, cmd, s, args);}
			if(args[0].equalsIgnoreCase("invite")){invite(p, cmd, s, args);}
			else if(!args[0].equalsIgnoreCase("create") && !args[0].equalsIgnoreCase("invite")){p.sendMessage(ChatColor.RED + "Invalid Command");}
		}
		
		
		return false;
	}

	public void help(Player p){
		p.sendMessage(ChatColor.GOLD + "A list of commands for SurvivalGroup");
		p.sendMessage(ChatColor.YELLOW + "/SurvivalGroup" + ChatColor.WHITE + " : The SurvivalGroup Command List");
		p.sendMessage(ChatColor.YELLOW + "/SurvivalGroup create [Name] <Desc>" + ChatColor.WHITE + " : Create a new Group");
		p.sendMessage(ChatColor.YELLOW + "/SurvivalGroup leave" + ChatColor.WHITE + " : Leaves your current Group");
		p.sendMessage(ChatColor.YELLOW + "/SurvivalGroup group" + ChatColor.WHITE + " : Displays your Group information");
	}
	
	public void leave(Player p, Command cmo, String s, String[] args){
		if(plugin.pdata.getString(pid.toString() + ".group") != null){
			p.sendMessage("You have left the " + plugin.pdata.getString(pid.toString() + ".group"));
			plugin.gdata.set(plugin.pdata.getString(pid.toString() + ".group"), null);
			plugin.pdata.set(pid.toString() + ".group", null);
			plugin.pdata.set(pid.toString() + ".is_leader", null);
		}else{
			p.sendMessage("You must be in a Group to leave it");
		}
	}
	
	public void base(Player p, Command cmd, String s, String[] args){
		if(args[0].equalsIgnoreCase("base")){
			if(plugin.pdata.getString(p.getUniqueId().toString() + ".home") != null){
				p.teleport(baseLocation(p.getUniqueId()));
				p.sendMessage(ChatColor.GOLD + "Teleporting To Base");
			}else{
				p.sendMessage(ChatColor.YELLOW + "You don't have a base set");
			}
		}
		if(args[0].equalsIgnoreCase("setbase")){
			setBase(p.getUniqueId());
			p.sendMessage("You have set your base");
		}
	}
	
	private static ItemStack Safe(String name){
		ItemStack safe = new ItemStack(Material.ENDER_CHEST, 1);
		ItemMeta meta = safe.getItemMeta();
		meta.setDisplayName(ChatColor.BLUE + name + "'s Safe");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("Place this in a chunk to set");
		lore.add("that chunk as the group base");
		meta.setLore(lore);
		safe.setItemMeta(meta);
		return safe;
	}
		
	
	public void create(Player p, Command cmd, String s, String[] args){
		if(args.length == 1){
			p.sendMessage("Correct Usage: /SurvivalGroup create [Name] <desc>");
		}
		if(args.length == 2){
			if(plugin.gdata.getString(args[1]) == null){
				if(plugin.pdata.getString(pid.toString() + ".group") == null){
					String name = args[1];
					this.desc = "Needs to be set using /SuvivalGroup setDesc [Name] [Desc]";
					plugin.pdata.set(pid.toString() + ".name", p.getName());
					plugin.pdata.set(pid.toString() + ".group", args[1]);
					plugin.pdata.set(pid.toString() + ".is_leader", true);
					plugin.gdata.set(name + ".leader", p.getName());
					plugin.gdata.set(name + ".leader_id", pid.toString());
					plugin.gdata.set(name + ".desc", this.desc);
					
					p.getInventory().addItem(Safe(args[1]));
					Bukkit.getServer().broadcastMessage("The Survival Group " + name + " has been formed");
					p.sendMessage("Place this down in your claim ONLY");
				}else{
					p.sendMessage("You must leave your current group before making a new one");
				}
			}else{
				p.sendMessage("That group already exists");
			}
		}else if(args.length >= 3){
			if(plugin.gdata.getString(args[1]) == null){
				if(plugin.pdata.getString(pid.toString() + ".group") == null){
					String name = args[1];
					plugin.pdata.set(pid.toString() + ".name", p.getName());
					plugin.pdata.set(pid.toString() + ".group", args[1]);
					plugin.pdata.set(pid.toString() + ".is_leader", true);
					plugin.gdata.set(name + ".leader", p.getName());
					plugin.gdata.set(name + ".leader_id", p.getUniqueId().toString());
					String d = args[2];
					int a = (args.length);
					for(int b = 3; b < a; b++){
						d = d + " " +
					  args[b];
					}
					this.desc = d;
					
					plugin.gdata.set(name + ".desc", this.desc);
									
					p.getInventory().addItem(new ItemStack(Safe(args[1])));
					Bukkit.getServer().broadcastMessage("The Survival Group " + name + " has been formed");
					p.sendMessage("Place this down in your claim ONLY");
					
				}else{
					p.sendMessage("You must leave your current group before making a new one");
				}
			}else{
				p.sendMessage("That group already exists");
			}	
		}else{
		}
	}

	
	public Location baseLocation(UUID pid){
		String[] str = plugin.pdata.getString(pid + ".home").split(",");
		Location loc = new Location(Bukkit.getWorld(str[0]), Double.parseDouble(str[1]), Double.parseDouble(str[2]), Double.parseDouble(str[3]));
		return loc;
	}
	
	public void setBase(UUID pid){
		Player TP = (Player)Bukkit.getServer().getPlayer(pid);
		Location loc = TP.getLocation();
		String location = loc.getWorld().getName() + "," + loc.getX() + "," + loc.getY() + "," + loc.getZ() + "," + loc.getYaw() + "," + loc.getPitch();
		plugin.pdata.set(pid + ".home", location);
	}

	public void group(Player p){
		if(plugin.pdata.getString(pid.toString() + ".group") != null){
			p.sendMessage(ChatColor.GOLD + plugin.pdata.getString(pid.toString() + ".group"));
			p.sendMessage(ChatColor.YELLOW + "Description" + ChatColor.WHITE + ": " + plugin.gdata.getString(plugin.pdata.getString(pid.toString() + ".group") + ".desc"));
			p.sendMessage(ChatColor.YELLOW + "Leader" + ChatColor.WHITE + ": " + plugin.gdata.getString(plugin.pdata.getString(pid.toString() + ".group") + ".leader"));
			p.sendMessage(ChatColor.DARK_AQUA + "Chunk" + ChatColor.WHITE + ":" + plugin.gdata.getString(plugin.pdata.getString(pid.toString() + ".group") + ".claim"));
		}else{
			p.sendMessage("You are not in a Group");
		}
	}
	
	public void invite(Player p, Command cmd, String s, String[] args){
		
		
	}
}



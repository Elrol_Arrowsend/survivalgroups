package com.elrol.survival_group.core;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.elrol.survival_group.commands.Commands;
import com.elrol.survival_group.commands.commandClass;
import com.elrol.survival_group.listeners.blockListener;
import com.elrol.survival_group.listeners.joinListener;
import com.elrol.survival_group.listeners.leaveListener;

public class Main extends JavaPlugin{

	public final Logger logger = Logger.getLogger("Minecraft");
	public Main plugin;
	
	public File playerData = new File(getDataFolder()+"/Data/playerData.yml");
	public FileConfiguration pdata = YamlConfiguration.loadConfiguration(playerData);
	
	public File groupData = new File(getDataFolder()+"/Data/groupData.yml");
	public FileConfiguration gdata = YamlConfiguration.loadConfiguration(groupData);
	
	
	@Override
	public void onEnable(){
		loadFiles();
		loadCommands();
		loadMethods();
		
	}
	
	@Override
	public void onDisable(){
		saveFiles();
	}
	
	public void saveFiles(){
		try {
			pdata.save(playerData);
			gdata.save(groupData);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void loadFiles(){
		if(!playerData.exists() || !groupData.exists()){
			try {
				gdata.save(groupData);
				pdata.save(playerData);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			try {
				pdata.load(playerData);
				gdata.load(groupData);
			} catch (IOException
					| InvalidConfigurationException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void loadMethods(){
		Bukkit.getServer().getPluginManager().registerEvents(new joinListener(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new leaveListener(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new blockListener(plugin), this);
	}
	
	public void loadCommands(){
		getCommand("survivalgroup").setExecutor(new Commands(this));
		//getCommand("survivalgroup create").setExecutor(new SurvivalGroup());
		getCommand("kit").setExecutor(new commandClass());
		
		
	}

}

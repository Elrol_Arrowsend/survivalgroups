package com.elrol.survival_group.listeners;

import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

import com.elrol.survival_group.core.Main;


public class blockListener implements Listener {
	private final Main plugin;
	
	public blockListener(Main plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event){
		Player p = (Player) event.getPlayer();		
		String pid = p.getUniqueId().toString();
		String group = plugin.pdata.getString(pid + ".group");
		String leaderUUID = plugin.gdata.getString(group + ".leader_id");
		
		if(event.getBlock().getType() == Material.ENDER_CHEST){
			
			if(leaderUUID == pid){
				Chunk claim = event.getBlock().getChunk();
				plugin.gdata.set(plugin.gdata.getString(plugin.pdata.getString(p.getUniqueId().toString() + ".group")) + ".claim", claim.toString());
			}
		}
		if(event.getBlock().getType() == Material.BEDROCK){
			if(!p.hasPermission("bplace")){
				event.setCancelled(true);
				p.sendMessage("cant place block");
				
				
			}	
		}
		
	}
}
